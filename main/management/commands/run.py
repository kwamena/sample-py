from django.core.management.base import BaseCommand
from main.utils.util import setup_driver, log_in, auth, scrap_emails, scrap_tasks, join_emails
from main.utils import config
from time import sleep
from main.utils.ssh import Ssh
from main.utils import util


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--pages', help="Number of pages to fetch")
        parser.add_argument('--session', help="Browser session")
        parser.add_argument('--fromDate', help="From Date")
        parser.add_argument('--toDate', help="To Date")

    def handle(self, *args, **options):
        driver = setup_driver(options['session'])
        if driver is None:
            return

        log_in(driver, config.url)
        auth(driver, config.url, options['session'] is not None)

        pages = 1
        if options['pages'] is not None:
            pages = int(options['pages'])

        from_date = options['fromDate']
        to_date = options['toDate']
        if from_date is not None and to_date is not None:
            url = config.page_url + config.page_filter.replace("fromDate", from_date).replace("toDate", to_date)
            driver.get(url)
            sleep(2)

        tasks = scrap_tasks(driver, pages, from_date, to_date)
        email_list = scrap_emails(driver, tasks)
        emails = join_emails(email_list)
        print("Email List")
        print(emails)

        if len(emails) == 0:
            return

        results = Ssh().query(emails)

        util.echo(results, "Ssh Results")
        filtered_emails = util.filter_existing(email_list, results)
        util.flag_tasks(driver, join_emails(filtered_emails), 1, 2)
