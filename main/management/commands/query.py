from django.core.management.base import BaseCommand
from main.utils.util import query_hosts, quote_emails
import json


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--emails', help="Comma seperated email list")

    def handle(self, *args, **options):
        emails = options['emails']

        if emails is None:
            print("No Emails specified")
            return

        payload = open("payload.json","r+")
        result_json = query_hosts(quote_emails(emails), json.loads(payload.read()))
        payload.close()

        print(json.dumps(result_json))