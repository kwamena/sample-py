from django.core.management.base import BaseCommand
from main.utils.util import setup_driver, log_in, auth, flag_tasks
from main.utils import config as cf


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--emails', help="Comma seperated email")
        parser.add_argument('--session', help="Browser session")

    def handle(self, *args, **options):
        emails = options['emails']

        print("Task infomation Required")
        print("========================")
        print("")
        print("Current Status Options: ")
        print("Success = 1, Error = 2")
        status = input("Enter Current Status: ")
        if int(status) == 1:
            print("")
            print("Action Taken Options: ")
            print("All user data deleted = 1, No user data found = 2, All user data retained = 3, Mix of user data delete & retained = 4")
            action = input("Enter Action Taken:")
        else:
            action = -1

        chrome_driver = setup_driver(options['session'])

        log_in(chrome_driver, cf.url)
        auth(chrome_driver, cf.url, options['session'] is not None)

        flag_tasks(chrome_driver, emails, int(status), int(action))
