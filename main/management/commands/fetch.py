from django.core.management.base import BaseCommand
from main.utils.util import setup_driver, log_in, auth, scrap_emails, scrap_tasks
from main.utils import config
from time import sleep


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--pages', help="Number of pages to fetch")
        parser.add_argument('--session', help="Browser session")
        parser.add_argument('--fromDate', help="From Date")
        parser.add_argument('--toDate', help="To Date")

    def handle(self, *args, **options):
        base_url = config.url
        driver = setup_driver(options['session'])
        if driver is None:
            return

        log_in(driver, base_url)
        auth(driver, base_url, options['session'] is not None)

        pages = 1
        if options['pages'] is not None:
            pages = int(options['pages'])

        from_date = None
        if options['fromDate'] is not None:
            from_date = options['fromDate']

        to_date = None
        if options['toDate'] is not None:
            to_date = options['toDate']

        if from_date is not None and to_date is not None:
            url = config.page_url +config.page_filter.replace("fromDate", from_date).replace("toDate", to_date)
            driver.get(url)
            sleep(2)

        tasks = scrap_tasks(driver, pages, from_date, to_date)
        emails = scrap_emails(driver, tasks)

        email_list = ""
        for i in range(0, len(emails)):
            email_list = email_list + emails[i]
            if i != len(emails)-1:
                email_list = email_list + ","
        print("Email List")
        print(email_list)
