from django.db import models


class Task(models.Model):
    email = models.CharField(max_length=100, unique=True)
    url = models.TextField()
    complete = models.BooleanField(default=False)

    def __str__(self):
        return "email:%s,  url:%s ...,  complete:%i" % (self.email, self.url[:20], self.complete)