
debug = True
live = False

url = "https://xxxxxxxx/myapps/tasks?page=1&order=u_notification_date"
remote = "http://127.0.0.1:4444/wd/hub"
duo_url = "https://saml.autodesk.com/idp"
myapps_url = "https://xxxxxxx/myapps"
page_url = "https://xxxxxx/myapps/tasks?"
page_filter = "filter=^sys_created_on>javascript%3Ags.dateGenerate('fromDate'%2C'23%3A59%3A59')^sys_created_on<javascript%3Ags.dateGenerate('toDate'%2C'23%3A59%3A59')"

cookies_file = "cookies.json"

chrome_driver = ""
firefox_driver = ""

xpath_error_500 = "//span[@id='error_code']"
xpath_user_options = "//a[@id='sign-in-block-profile' and @data-toggle='dropdown']"
xpath_my_tasks = "//a[@id='nav-signed-mytasks']"
xpath_pages = "//a[contains(@class,'pn-item')]"
xpath_cur_page = "//a[contains(@class,'pn-item') and contains(@class,'active')]"

didge_cmd = "cd %s && source ../venv/bin/activate && ./manage.py query --email %s"
