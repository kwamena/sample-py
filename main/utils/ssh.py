from paramiko import SSHClient, SSHConfig
from main.utils import access
from main.utils import config as cf
from main.utils import util


class Ssh:
    ssh_client = None

    def __init__(self):
        self.ssh_client = SSHClient()
        self.ssh_client.load_system_host_keys()
        key_path = access.didge_ssh_path + "/" + access.didge_private_key
        self.ssh_client.connect(access.didge_ip, username=access.didge_user, key_filename=key_path)

    def query(self, emails):
        basket = ""
        cmd = cf.didge_cmd % (access.didge_source, emails)
        util.echo("didge-cmd")
        util.echo(cmd)
        stdin, stdout, stderr = self.ssh_client.exec_command(cmd)
        for i, line in enumerate(stdout):
            line = line.rstrip()
            basket += line

        self.ssh_client.close()
        return basket
