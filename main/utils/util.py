from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from main.utils import access
from time import sleep
from main.models import Task
from main.utils import config
import json

def setup_driver(session_id):

    try:
        driver = webdriver.Remote(command_executor=config.remote,desired_capabilities=DesiredCapabilities.FIREFOX)
    except:
        print("Oops, looks like the 'selenuum standalone server' is not running.")
        print("Run command below in a new terminal window or tab first.  Java Requires.")
        print("java -jar drivers/selenium-server-standalone-3.14.0.jar")
        if session_id is not None:
            print("")
            print("If a selenium standalone server is running then check your session ")
        return None

    if session_id is not None:
        driver.close()
        driver.session_id = session_id

    print("Session: " + str(driver.session_id))

    driver.set_window_position(0, 0)
    driver.set_window_size(1024, 800)

    driver.set_page_load_timeout(600)

    return driver


def log_in(driver, base_url):
    if config.duo_url in driver.current_url or config.myapps_url in driver.current_url:
        print("Skipping SSO access credentials")

    else:
        print("Performing SSO access credentials")

        driver.get(base_url)

        user_name = driver.find_element_by_xpath("//input[@id='userName']")
        if user_name is not None:
            user_name.send_keys(access.email)

        next_button = driver.find_element_by_xpath("//button[@id='verify_user_btn']")
        if next_button is not None:
            next_button.click()


def auth(driver, base_url, existing_session):
    if config.duo_url in driver.current_url or config.myapps_url in driver.current_url:
        print("Skipping Basic Auth")
    else:
        print("Performing Basic Auth")

        window_handler = driver.window_handles[0]

        WebDriverWait(driver, 10).until(EC.alert_is_present())
        sleep(1)
        alert = driver.switch_to_alert()
        alert.send_keys(access.user + Keys.TAB + access.password)
        sleep(1)
        alert.accept()
        sleep(1)
        driver.switch_to.default_content()
        driver.switch_to_window(window_handler)

        sleep(5)

        setup_driver(driver.session_id)

        sleep(3)
        iframe = ""

        try:
            iframe = driver.find_element_by_xpath("//iframe")
            driver.switch_to.frame(iframe)
            echo("iframe is here")
        except:
            echo("iframe not here")

        sleep(1)
        try:
            push_button = driver.find_element_by_xpath("//div[@class='row-label push-label']")
            push_button.click()
            echo("pushbutton is here")
        except:
            echo("pushbutton not here")

        driver.switch_to.default_content()

    if not existing_session:
        push_auth_wait()

    try:
        user_options = driver.find_element_by_xpath(config.xpath_user_options)
        if user_options is not None:
            user_options.click()

        my_tasks = driver.find_element_by_xpath(config.xpath_my_tasks)
        if my_tasks is not None:
            my_tasks.click()
    except:
        echo("No Error 500")

    sleep(5)

    try:
        yes_all_button = driver.find_element_by_xpath("//button[@id='adsk-eprivacy-yes-to-all']")
        if yes_all_button is not None:
            yes_all_button.click()
    except:
        echo("No Yes All")

    try:
        continue_button = driver.find_element_by_xpath("//button[@id='adsk-eprivacy-continue-btn']")
        if continue_button is not None:
            continue_button.click()
    except:
        echo("No continue")


def push_auth_wait():
    input("Press [Enter] after Authenticating Push on Mobile & Profile is reloaded")
    sleep(5)
    pass


def scrap_tasks(driver, pages, from_date, to_date):
    tasks = []
    while True:

        elements = driver.find_elements(By.XPATH, "//tbody/tr/td/a[contains(@href,'/update_task/')]")
        for element in elements:
            tasks.append(element.get_attribute("href"))

        element_cur_page = None
        try:
            element_cur_page = driver.find_element_by_xpath(config.xpath_cur_page)
        except:
            print("No emails found")
            break

        if element_cur_page is not None:
            cur_page = get_page_index(element_cur_page.get_attribute("href"))
            if cur_page < pages:
                element_pages = driver.find_elements(By.XPATH, config.xpath_pages)
                found_next=False
                for element_page in element_pages:
                    page_index = get_page_index(element_page.get_attribute("href")) #int(element_page.get_attribute("href").replace(config.page_url,""))
                    if page_index > cur_page:
                        if page_index <= pages:
                            found_next = True
                            element_page.click()
                        break

                if not found_next:
                    break
            else:
                break

    return tasks


def get_page_index(href):
    params = href.replace(config.page_url, "")
    params_list = params.split("&")
    cur_page = 1
    for param in params_list:
        if "page=" in param:
            cur_page = int(param.replace("page=", ""))
            break
    return cur_page


def scrap_emails(driver, urls):
    emails = []
    for url in urls:
        driver.get(url)
        WebDriverWait(driver, 2).until(lambda driver: driver.current_url == url)
        email = (driver.find_elements(By.XPATH, "//p[@class='margin-top-1']")[1]).text
        query_set = Task.objects.filter(email=email)

        if query_set.count() == 0:
            task = Task()
            task.email = email
        else:
            task = query_set.first()

        task.url = url
        task.save()

        emails.append(email)
    return emails


def flag_tasks(driver, email_list, status, action):
    emails = email_list.split(",")
    for email in emails:
        query_set = Task.objects.filter(email=email)
        if query_set.count() > 0:
            task = query_set.first()
            driver.get(task.url)
            WebDriverWait(driver, 2).until(lambda driver: driver.current_url == task.url)
            sleep(1)

            try:
                select_status(driver, status)
                if status == 1:
                    select_action(driver, action)

                save_button = driver.find_element_by_xpath("//button[@class='x-btn-pri btn--consistent']")
                if save_button is not None:
                    if config.live:
                        save_button.click()
                echo("Flagged. '" + email + "'!")
                sleep(3)
            except:
                echo("Oops. couldn't close '" + email + "'!")

    # sleep(15)


def select_status(driver, status):
    element = driver.find_element_by_xpath("//span[@class='select2 select2-container select2-container--forge']")
    element.click()

    elements = driver.find_elements(By.XPATH,"//li[contains(@class,'select2-results__option')]")
    if elements is not None:
        (elements[status-1]).click()


def select_action(driver, action):
    element = driver.find_element_by_xpath("//span[contains(@id,'select2-task-status-type-container')]")
    element.click()

    elements = driver.find_elements(By.XPATH,"//li[contains(@id, 'select2-task-status-type-result-')]")
    if elements is not None:
        (elements[action-1]).click()


def join_emails(emails):
    email_list = ""
    for i in range(0, len(emails)):
        email_list = email_list + emails[i]
        if i != len(emails) - 1:
            email_list = email_list + ","
    return email_list


def quote_emails(emails):
    email_list = emails.split(",")
    email_result = ""
    for i in range(0, len(email_list)):
        email_result = email_result + "'" + email_list[i] + "'"
        if i != len(email_list) - 1:
            email_result = email_result + ","
    return email_result;


def query_hosts(users, hosts):
    host_list = []
    for host in hosts:
        host_dx = {'name':host["name"]}
        database_list = []
        for database in host["databases"]:
            try:
                database_list.append(query_database(users, host["ip"], database))
            except:
                echo("Error accessing: %s" % database["name"])
        host_dx['databases'] = database_list
        host_list.append(host_dx)

    return host_list


def query_database(users, host_ip, database):
    try:
        import mysql.connector
    except:
        echo("mysql.connector import failed. ignore if not on didge")
    conn = mysql.connector.connect(host=host_ip, database=database["name"], user=access.mysql_user, password=access.mysql_password)
    cursor = conn.cursor(buffered=True)
    database_dx = {'name': database["name"]}

    table_list = []
    for table in database["tables"]:
        table_list.append(query_users_in_table(cursor, table, users))
    cursor.close()
    conn.close()

    database_dx['tables'] = table_list
    return database_dx


def query_users_in_table(cursor, table, users):
    cursor.execute("select distinct(%s) from %s where %s in (%s)" % (table["field"], table["name"], table["field"], users))
    emails = []
    dx = {}
    for row in cursor.fetchall():
        if len(row[0]) > 0:
            emails.append(row[0])

    dx["name"] = table["name"]
    dx["field"] = table["field"]
    if len(emails) > 0:
        dx["emails"] = join_emails(emails)
    else:
        dx["emails"] = ""

    return dx


def filter_existing(emails, result):
    print("Emails found in databases")
    print("===============================")
    purge_list = ""
    for host in json.loads(result):
        for database in host["databases"]:
            for table in database["tables"]:
                if len(table["emails"]) > 0:
                    print("emails(%s) found in --field(%s) --table(%s) --database(%s) --host(%s)" % (table["emails"], table["field"], table["name"], database["name"], host["name"]))
                    if len(purge_list) > 0:
                        purge_list = purge_list + "," + table["emails"]
                    else:
                        purge_list = purge_list + table["emails"]

    if purge_list == "":
        print("None")

    for purge_item in purge_list.split(","):
        if len(purge_item) > 0:
            emails.remove(purge_item)

    return emails


def echo(str, title=None):
    if config.debug:
        if title is not None:
            print(title)
        print(str)
