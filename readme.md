## Autodesk GDPR Purge
Script designed to parse the Auto Ticket website for tickets (using selenium),
Search our servers & databases for traces of emails (ssh into our remote servers),
Report tickets that require human intervention and Closes tickets that don't which are most

#### Configuration
##### source 
Checkout one version of source on your location and another on remote server so they can communicate with each other
3 Legged Authentication with Push Notification makes it impossible to run completely remote.

##### local remote data exchange format
````json
[{"name": "XXXX", "databases": [{"name": "xxxxx", "tables": [{"field": "xxxx", "emails": "", "table": "xxxx"}, {"field": "yyyy", "emails": "", "table": "yyyyy"}]}]},{"name": "XXXX", "databases": [{"name": "zzzz", "tables": [{"field": "cccc", "emails": "", "table": "vvvv"}, {"field": "ffff", "emails": "", "table": "bbbb"}]}]}]
````

##### payload.json
payload.json contains server databases, table & fields to be searched
```json
[{"name":"Server1","ip":"XX.XX.XX.XX","databases":[{"name":"db1","tables":[{"name":"tbl1","field":"col1"},{"name":"tb1","field":"col1"},{"name":"tbl2","field":"col1"}]}]},{"name":"Server2","ip":"YY.YY.YY.YY","databases":[{"name":"db1","tables":[{"name":"tbl1","field":"col1"},{"name":"tb1","field":"col1"},{"name":"tbl2","field":"col1"}]}]}]
```

##### main/utils/access.py 
Contains Ticket system access
```
email=""
user=""
password=""
```

MySQL Access
```
mysql_user=""
mysql_password=""
```

Remote Server Ssh Access
```
didge_ip=""
didge_user=""
didge_ssh_path=""
didge_private_key=""
```

Remote Source access
```
didge_source="/home/xxxxxx/autodesk/autodesk-gdpr-purge"
```
##### main/utils/config.py
When set to True 'debug' will display logs whiles it runs

```python
debug = True
```

When set to True 'live' will close tickets
```python
live = False
```


### Installation 
You are kindly advised to use virtualenv
```bash
pip install -r requirements
```

#### run Selenium RC
To be run first before fetch, flag or run
```bash
java -jar drivers/selenium-server-standalone-3.14.0.jar
```


#### fetch emails
```bash
./manage.py fetch --pages 2  
```

#### fetch emails
```bash
./manage.py fetch --pages 2 --sesssion 033a5bc8-ee31-3b4c-9aec-dfeca0d323fc
```

#### fetch emails
```bash
./manage.py fetch --pages 2 --fromDate 2018-07-01 --toDate 2018-07-14
```

#### query servers for emails
```bash
./manage.py query --emails 'a@b.c,d@e.f'
```

#### close tickets 
```bash
./manage.py flag --emails 'a@b.c,d@e.f' --sesssion 033a5bc8-ee31-3b4c-9aec-dfeca0d323fc
```

#### run 
fetch, query & close tickets 
```bash
./manage.py run --pages 1 --fromDate 2018-07-01 --toDate 2018-07-14
```

#### run 
fetch, query & close tickets 
```bash
./manage.py run --pages 1 --fromDate 2018-07-14 --toDate 2018-07-17 --session 1fae800a-c4d2-6941-a53d-411aab9e6668
```