import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "purge.settings")
django.setup()

from main.utils.util import quote_emails

def test_quote_emails():
    emails = "a@b.c, d@e.f"
    email_list = quote_emails(emails)
    assert len(email_list) == 16
    assert "a@b.c" in email_list
    assert "d@e.f" in email_list
